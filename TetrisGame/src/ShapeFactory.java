/**
 * generates a shape depending on the shape Id and it's initial rotation Id
 * 
 * @author SRINIVASA REDDY B.R (2007119)
 * @version 1.0, Apr 13, 2008
 */
public class ShapeFactory {

	public static Shape generateShape(int shapeID, int rotationID) {
		Shape shape = null;
		Rotate rotate = new Rotate();
		switch (shapeID) {

		case 1:
			shape = new BoxShape();
			rotate.rotateNTimes(shape, rotationID);
			break;

		case 2:
			shape = new InvertedLShape();
			rotate.rotateNTimes(shape, rotationID);
			break;

		case 3:
			shape = new InvertedZShape();
			rotate.rotateNTimes(shape, rotationID);
			break;

		case 4:
			shape = new IShape();
			rotate.rotateNTimes(shape, rotationID);
			break;

		case 5:
			shape = new LShape();
			rotate.rotateNTimes(shape, rotationID);
			break;

		case 6:
			shape = new TShape();
			rotate.rotateNTimes(shape, rotationID);
			break;

		case 0:
			shape = new ZShape();
			rotate.rotateNTimes(shape, rotationID);
			break;

		}

		return shape;
	}
}
