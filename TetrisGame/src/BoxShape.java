/** This class represents box shape and has a predefined shape bit pattern
 * 
 */

/**
 * @author Admin
 * 
 */
public class BoxShape extends Shape {

	public BoxShape() {
		Integer[][] theShapeBitPattern = { { 1, 1, 0, 0 }, { 1, 1, 0, 0 },
				{ 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

		super.setShapeBitPattern(theShapeBitPattern);
	}

	public static void main(String[] args) {
		BoxShape b = new BoxShape();
		b.display(b.getShapeBitPattern());
	}

	public void display(Integer[][] arr) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();

		}
	}
}