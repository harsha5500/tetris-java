/**
 * returns a move object depending on the key press
 */
import java.awt.event.KeyEvent;

public class MoveFactory {

	static Controller controller;

	static KeyConfig keyConfig;

	static Move m;

	static int xPos, yPos;

	static int k;

	public static Move getMoveObject(KeyEvent ke) {

		controller = Controller.getInstance();
		keyConfig = KeyConfig.getInstance();
		xPos = controller.getShapeLocation().getX();
		yPos = controller.getShapeLocation().getY();
		k = ke.getKeyCode();

		// enter only if  game is in resume state
		if (controller.getPauseResumeEndFlag() == 2) {

			if (k == keyConfig.getLeft()) {
				m = new Left();
				m.movement();
			}

			else if (k == KeyEvent.VK_DOWN) {
				m = new Fall();
				m.movement();
			}

			else if (k == keyConfig.getRight()) {
				m = new Right();
				m.movement();
			}

			else if (k == KeyEvent.VK_SHIFT) {
				m = new Up();
				m.movement();
			}

			else if (k == keyConfig.getDrop()) {
				m = new Drop();
				m.movement();
			}

			else if (k == keyConfig.getRotate()) {
				if (xPos <= MyGamePanel.XCells
						- Util.getShapeHeight(controller.getCurrentShape()))
					new Rotate().rotate(controller.getCurrentShape());
			}

			else if (k == KeyEvent.VK_SPACE) {
				controller.setPauseResumeEndFlag(1);

			}
		}

		// if the game is paused and pause key is pressed then resume the game
		else if (controller.getPauseResumeEndFlag() == 1) {
			if (k == KeyEvent.VK_SPACE)
				controller.setPauseResumeEndFlag(2);
		}

		// return move object 
		return m;

	}

}
