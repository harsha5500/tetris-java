/**
 *  This Class provides user interface and methods to start game , load game , save game, 
 *  This class contains main method to run the game 
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle;

/**
 * 
 * @author iiitb
 */

public class GameGUI extends javax.swing.JFrame {

	private ScoreCard scoreCard;

	private Controller controller;

	private GameSetting gameSetting;

	private KeyConfig keyConfig;

	public ProgressGame t;

	private static int OFF = 0;

	private static int ON = 1;

	private int gameActionListener = GameGUI.OFF;

	/** Creates new form GUI2 */

	public GameGUI() {
		gameSetting = GameSetting.getInstance();
		gameSetting.readFileAndSetGameSettings();

		keyConfig = KeyConfig.getInstance();
		keyConfig.readFileAndSetKeyConfig();

		scoreCard = ScoreCard.getInstance();
		scoreCard.setGameGUI(this);

		controller = Controller.getInstance();
		/*
		 * scoreCard.initializeScoreCard(gameSetting.getStartLevel());
		 * controller.initializeController(gameSetting.getStartLevel());
		 */
		initComponents();
		setTitle("TETRIS !!!");
		setBounds(100, 100, 800, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent ke) {
				if (gameActionListener == GameGUI.ON) {
					MoveFactory.getMoveObject(ke);
					gamePanel.repaint();
					scorePanel.repaint();
				}

			}

			public void keyReleased(KeyEvent ke) {
			}

			public void keyTyped(KeyEvent ke) {

			}
		});

	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * 
	 */

	private void initComponents() {

		menuBar1 = new java.awt.MenuBar();
		menu1 = new java.awt.Menu();
		jMenuItem1 = new javax.swing.JMenuItem();
		jInternalFrame2 = new javax.swing.JInternalFrame();
		jPanel1 = new javax.swing.JPanel();
		jPanel2 = new javax.swing.JPanel();
		scorePanel = new javax.swing.JPanel();
		nextShapePanel = new javax.swing.JPanel();
		scoreLabel = new javax.swing.JLabel();
		scoreLabelValue = new javax.swing.JLabel();
		linesLabel = new javax.swing.JLabel();
		linesLabelValue = new javax.swing.JLabel();
		levelLabel = new javax.swing.JLabel();
		levelLabelValue = new javax.swing.JLabel();
		jMenuBar1 = new javax.swing.JMenuBar();
		jMenu1 = new javax.swing.JMenu();
		jMenu1.setRolloverEnabled(true);
		jMenu1.setFocusPainted(true);

		// loads a game
		jMenuItem3 = new javax.swing.JMenuItem();
		jMenuItem3.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(final MouseEvent arg0) {
				loadGame();
			}
		});

		// saves a game
		jMenuItem4 = new javax.swing.JMenuItem();
		jMenuItem4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(final MouseEvent arg0) {
				saveGame();
			}
		});

		// pauses a game
		jMenuItem7 = new javax.swing.JMenuItem();
		jMenuItem7.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				if (controller.getPauseResumeEndFlag() == 2) {
					controller.setPauseResumeEndFlag(1);
				} else
					controller.setPauseResumeEndFlag(2);
			}
		});

		// starts a new game
		jMenuItem2 = new javax.swing.JMenuItem();
		jMenuItem2.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				System.out.println("startin new game..");
				startNewGame();

			}
		});

		jMenuItem2.setBackground(Color.BLACK);
		jMenuItem2.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem2.setText("New Game");
		jMenu1.add(jMenuItem2);

		final JMenuItem endGameMenuItem = new JMenuItem();
		endGameMenuItem.setForeground(Color.WHITE);
		endGameMenuItem.setBackground(Color.BLACK);
		endGameMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				controller.setPauseResumeEndFlag(3);
			}
		});
		endGameMenuItem.setText("End Game");
		jMenu1.add(endGameMenuItem);
		jMenuItem5 = new javax.swing.JMenuItem();
		jMenuItem6 = new javax.swing.JMenuItem();
		jMenu2 = new javax.swing.JMenu();
		jMenuItem8 = new javax.swing.JMenuItem();
		jMenu4 = new javax.swing.JMenu();
		jMenuItem9 = new javax.swing.JMenuItem();
		jMenuItem10 = new javax.swing.JMenuItem();
		jMenuItem11 = new javax.swing.JMenuItem();

		menu1.setLabel("Menu");
		menuBar1.add(menu1);

		jInternalFrame2.setBackground(new java.awt.Color(0, 0, 51));
		jInternalFrame2.setResizable(true);
		jInternalFrame2.setVisible(true);

		javax.swing.GroupLayout jInternalFrame2Layout = new javax.swing.GroupLayout(
				jInternalFrame2.getContentPane());
		jInternalFrame2.getContentPane().setLayout(jInternalFrame2Layout);
		jInternalFrame2Layout.setHorizontalGroup(jInternalFrame2Layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 278, Short.MAX_VALUE));
		jInternalFrame2Layout.setVerticalGroup(jInternalFrame2Layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 348, Short.MAX_VALUE));

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setBackground(new java.awt.Color(0, 0, 0));
		setForeground(new java.awt.Color(0, 0, 51));
		setResizable(false);

		jPanel1.setBackground(new java.awt.Color(0, 0, 0));

		jPanel2.setBackground(Color.BLACK);

		scorePanel.setBackground(new java.awt.Color(0, 0, 0));
		scorePanel.setBorder(javax.swing.BorderFactory
				.createLineBorder(new java.awt.Color(102, 102, 102)));
		scorePanel.setForeground(new java.awt.Color(255, 255, 255));

		nextShapePanel.setBackground(new java.awt.Color(102, 102, 102));

		scoreLabel.setForeground(new java.awt.Color(255, 255, 255));
		scoreLabel.setText("Score");

		scoreLabelValue.setForeground(new java.awt.Color(255, 255, 255));
		scoreLabelValue.setText(" ");

		linesLabel.setForeground(new java.awt.Color(255, 255, 255));
		linesLabel.setText("Lines");

		linesLabelValue.setForeground(new java.awt.Color(255, 255, 255));
		linesLabelValue.setText(" ");

		levelLabel.setForeground(new java.awt.Color(255, 255, 255));
		levelLabel.setText("Level");

		levelLabelValue.setForeground(new java.awt.Color(255, 255, 255));
		levelLabelValue.setText(" ");

		jMenuBar1.setBackground(new java.awt.Color(0, 0, 0));

		jMenu1.setBackground(new java.awt.Color(0, 0, 0));
		jMenu1.setForeground(new java.awt.Color(255, 255, 255));
		jMenu1.setText("File");

		jMenuItem3.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem3.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem3.setText("Load Game");
		jMenu1.add(jMenuItem3);

		jMenuItem4.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem4.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem4.setText("Save Game");
		jMenu1.add(jMenuItem4);

		jMenuItem7.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem7.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem7.setText("Pause");
		jMenuItem7.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jMenuItem7MouseClicked(evt);
			}

			@Override
			public void mousePressed(java.awt.event.MouseEvent evt) {
				jMenuItem7MousePressed(evt);
			}

			@Override
			public void mouseReleased(java.awt.event.MouseEvent evt) {
				jMenuItem7MouseReleased(evt);
			}
		});
		jMenu1.add(jMenuItem7);

		jMenuItem5.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem5.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem5.setText("Scores");
		jMenuItem5.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mousePressed(java.awt.event.MouseEvent evt) {
				jMenuItem5MousePressed(evt);
			}

			@Override
			public void mouseReleased(java.awt.event.MouseEvent evt) {
				jMenuItem5MouseReleased(evt);
			}
		});
		jMenu1.add(jMenuItem5);

		jMenuItem6.setBackground(Color.BLACK);
		jMenuItem6.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem6.setText("Exit");
		jMenuItem6.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mousePressed(java.awt.event.MouseEvent evt) {
				jMenuItem6MousePressed(evt);
			}
		});
		jMenu1.add(jMenuItem6);

		jMenuBar1.add(jMenu1);

		jMenu2.setBackground(new java.awt.Color(0, 0, 0));
		jMenu2.setForeground(new java.awt.Color(255, 255, 255));
		jMenu2.setText("Options");

		jMenuItem8.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem8.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem8.setText("Settings");
		jMenuItem8.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jMenuItem8MouseClicked(evt);
			}

			@Override
			public void mouseReleased(java.awt.event.MouseEvent evt) {
				jMenuItem8MouseReleased(evt);
			}
		});
		jMenu2.add(jMenuItem8);

		jMenuBar1.add(jMenu2);

		jMenu4.setBackground(new java.awt.Color(0, 0, 0));
		jMenu4.setForeground(new java.awt.Color(255, 255, 255));
		jMenu4.setText("Help");

		jMenuItem9.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem9.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem9.setText("About");
		jMenu4.add(jMenuItem9);

		jMenuItem10.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem10.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem10.setText("Credits");
		jMenu4.add(jMenuItem10);

		jMenuItem11.setBackground(new java.awt.Color(0, 0, 0));
		jMenuItem11.setForeground(new java.awt.Color(255, 255, 255));
		jMenuItem11.setText("How to Play");
		jMenu4.add(jMenuItem11);

		jMenuBar1.add(jMenu4);

		setJMenuBar(jMenuBar1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(jPanel1,
				GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addComponent(jPanel1,
				GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 495,
				Short.MAX_VALUE));

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
				scorePanel);
		jPanel3Layout
				.setHorizontalGroup(jPanel3Layout
					.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel3Layout
									.createSequentialGroup()
									.addContainerGap()
									.addGroup(
											jPanel3Layout
													.createParallelGroup(
															GroupLayout.Alignment.TRAILING)
													.addGroup(
															jPanel3Layout
																	.createSequentialGroup()
																	.addGroup(
																			jPanel3Layout
																					.createParallelGroup(
																							GroupLayout.Alignment.TRAILING)
																					.addComponent(
																							linesLabel)
																					.addComponent(
																								scoreLabel)
																						.addComponent(
																								levelLabel))
																		.addPreferredGap(
																				LayoutStyle.ComponentPlacement.RELATED,
																				121,
																				Short.MAX_VALUE)
																		.addGroup(
																				jPanel3Layout
																						.createParallelGroup(
																								GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								linesLabelValue)
																						.addComponent(
																								levelLabelValue)
																						.addComponent(
																								scoreLabelValue))
																		.addGap(
																				60,
																				60,
																				60))
														.addGroup(
																jPanel3Layout
																		.createSequentialGroup()
																		.addComponent(
																				nextShapePanel,
																				GroupLayout.PREFERRED_SIZE,
																				195,
																				GroupLayout.PREFERRED_SIZE)
																		.addContainerGap()))));
		jPanel3Layout
				.setVerticalGroup(jPanel3Layout
						.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel3Layout
										.createSequentialGroup()
										.addGap(30, 30, 30)
										.addComponent(nextShapePanel,
												GroupLayout.PREFERRED_SIZE,
												183, GroupLayout.PREFERRED_SIZE)
										.addGap(57, 57, 57)
										.addGroup(
												jPanel3Layout
														.createParallelGroup(
																GroupLayout.Alignment.BASELINE)
														.addComponent(
																scoreLabelValue)
														.addComponent(
																scoreLabel))
										.addGap(19, 19, 19)
										.addGroup(
												jPanel3Layout
														.createParallelGroup(
																GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel3Layout
																		.createSequentialGroup()
																		.addGap(
																				83,
																				83,
																				83)
																		.addGroup(
																				jPanel3Layout
																						.createParallelGroup(
																								GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								levelLabelValue)
																						.addComponent(
																								levelLabel)))
														.addGroup(
																jPanel3Layout
																		.createSequentialGroup()
																		.addGap(
																				24,
																				24,
																				24)
																		.addGroup(
																				jPanel3Layout
																						.createParallelGroup(
																								GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								linesLabelValue)
																						.addComponent(
																								linesLabel))))
										.addContainerGap(262, Short.MAX_VALUE)));
		scorePanel.setLayout(jPanel3Layout);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGap(0, 362, Short.MAX_VALUE));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGap(0, 612, Short.MAX_VALUE));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(
				GroupLayout.Alignment.TRAILING).addGroup(
				jPanel1Layout.createSequentialGroup().addGap(31, 31, 31)
						.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE).addPreferredGap(
								LayoutStyle.ComponentPlacement.RELATED, 169,
								Short.MAX_VALUE).addComponent(scorePanel,
								GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout.createParallelGroup(
						GroupLayout.Alignment.LEADING).addComponent(scorePanel,
						GroupLayout.Alignment.TRAILING,
						GroupLayout.DEFAULT_SIZE, 668, Short.MAX_VALUE)
						.addGroup(
								jPanel1Layout.createSequentialGroup().addGap(
										29, 29, 29).addComponent(jPanel2,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE).addGap(27,
										27, 27)));
		jPanel1.setLayout(jPanel1Layout);
		getContentPane().setLayout(layout);
		pack();

	}// </editor-fold>//GEN-END:initComponents

	private void jMenuItem7MousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem7MousePressed

		// TODO add your handling code here:
	}// GEN-LAST:event_jMenuItem7MousePressed

	private void jMenuItem7MouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem7MouseClicked

		// jMenuItem7.setText(text);// TODO add your handling code here:
	}// GEN-LAST:event_jMenuItem7MouseClicked

	String text;

	// jMenuItem7.getText();

	private void jMenuItem7MouseReleased(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem7MouseReleased
		if (jMenuItem7.getText().equalsIgnoreCase("Pause"))
			jMenuItem7.setText("Resume");
		else
			jMenuItem7.setText("Pause");
		// TODO add your handling code here:
	}// GEN-LAST:event_jMenuItem7MouseReleased

	private void jMenuItem8MouseClicked(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem8MouseClicked
		// TODO add your handling code here:
	}// GEN-LAST:event_jMenuItem8MouseClicked

	private void jMenuItem8MouseReleased(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem8MouseReleased
		// TODO add your handling code here:
		this.setVisible(true);
		SettingsGUI frame = new SettingsGUI();
		frame.setVisible(true);
	}// GEN-LAST:event_jMenuItem8MouseReleased

	private void jMenuItem5MouseReleased(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem5MouseReleased

		this.setVisible(true);
		HighScores highScores = new HighScores();
		highScores.displayHighScores();
	}

	private void jMenuItem5MousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem5MousePressed
		// TODO add your handling code here:
	}// GEN-LAST:event_jMenuItem5MousePressed

	private void jMenuItem6MousePressed(java.awt.event.MouseEvent evt) {// GEN-FIRST:event_jMenuItem6MousePressed

		// int YES_NO_OPTION ;
		int n = JOptionPane.showConfirmDialog(new JFrame(),
				"Do you really want to Quit ? \n", "Inane warning",
				JOptionPane.YES_NO_OPTION);
		if (n == 0)
			System.exit(1); // TODO add your handling code here:
	}// GEN-LAST:event_jMenuItem6MousePressed

	/**
	 * @param args
	 *            the command line arguments
	 */

	// starts a new game
	public void startNewGame() {

		scoreCard.initializeScoreCard(gameSetting.getStartLevel());
		controller.initializeController(gameSetting.getStartLevel());
		// jPanel1.remove(jPanel2);
		gamePanel = new MyGamePanel(controller);
		gamePanel.setLayout(null);
		// gamePanel.setBounds(500, 500, 500, 500);
		// this.add(jPanel2);
		// jPanel1.add(jPanel2);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout.createSequentialGroup().addGap(0, 0, 0)
						.addComponent(gamePanel, GroupLayout.PREFERRED_SIZE,
								350, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(0, Short.MAX_VALUE)));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout.createSequentialGroup().addGap(0, 0, 0)
						.addComponent(gamePanel, GroupLayout.PREFERRED_SIZE,
								600, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(0, Short.MAX_VALUE)));
		jPanel2.setLayout(jPanel2Layout);

		if (gameSetting.getPreviewFlag()) {
			PreviewPanel panel;
			panel = new PreviewPanel(controller);
			panel.setBackground(Color.BLACK);

			javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
					nextShapePanel);
			jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(
					GroupLayout.Alignment.LEADING).addGroup(
					jPanel4Layout.createSequentialGroup().addContainerGap()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE,
									173, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(GroupLayout.DEFAULT_SIZE,
									Short.MAX_VALUE)));
			jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
					GroupLayout.Alignment.LEADING).addGroup(
					jPanel4Layout.createSequentialGroup().addContainerGap()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE,
									160, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(GroupLayout.DEFAULT_SIZE,
									Short.MAX_VALUE)));
			nextShapePanel.setLayout(jPanel4Layout);
		}

		this.repaint();
		// jPanel1.repaint();
		jPanel2.repaint();
		scorePanel.repaint();
		// this.setVisible(true);

		// startGameDisplay();
		// displayAll();
		updateScoreBoard();
		gameActionListener = GameGUI.ON;
		t = new ProgressGame(gamePanel);
	}

	// loads a new game
	public void loadGame() {
		// scoreCard.initializeScoreCard(gameSetting.getStartLevel());
		// controller.initializeController(gameSetting.getStartLevel());
		scoreCard.initializeScoreCard();

		Controller loadedController = (Controller) Util.load("controller.txt");
		controller.initializeController(loadedController);
		this.controller = loadedController;
		MoveFactory.controller = loadedController;

		gamePanel = new MyGamePanel(controller);
		gamePanel.setLayout(null);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout.createSequentialGroup().addGap(0, 0, 0)
						.addComponent(gamePanel, GroupLayout.PREFERRED_SIZE,
								//chg
								460, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(0, Short.MAX_VALUE)));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(
				GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout.createSequentialGroup().addGap(0, 0, 0)
						.addComponent(gamePanel, GroupLayout.PREFERRED_SIZE,
								600, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(0, Short.MAX_VALUE)));
		jPanel2.setLayout(jPanel2Layout);

		// if (gameSetting.getPreviewFlag())
		{
			PreviewPanel panel;
			panel = new PreviewPanel(controller);
			panel.setBackground(Color.BLACK);

			javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
					nextShapePanel);
			jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(
					GroupLayout.Alignment.LEADING).addGroup(
					jPanel4Layout.createSequentialGroup().addContainerGap()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE,
									173, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(GroupLayout.DEFAULT_SIZE,
									Short.MAX_VALUE)));
			jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
					GroupLayout.Alignment.LEADING).addGroup(
					jPanel4Layout.createSequentialGroup().addContainerGap()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE,
									160, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(GroupLayout.DEFAULT_SIZE,
									Short.MAX_VALUE)));
			nextShapePanel.setLayout(jPanel4Layout);
		}

		this.repaint();
		// jPanel1.repaint();
		jPanel2.repaint();
		scorePanel.repaint();
		// this.setVisible(true);

		// startGameDisplay();
		// displayAll();
		updateScoreBoard();
		gameActionListener = GameGUI.ON;
		t = new ProgressGame(gamePanel);

	}

	// saves the current game
	private void saveGame() {
		System.out.println("saving...");
		Util.store(Controller.getInstance(), "controller.txt");
		scoreCard.saveKeyConfigToFile();
		// Util.store(ScoreCard.getInstance(), "scoreCard.txt");

	}

	// update the score card on the GUI
	public void updateScoreBoard() {
		scoreLabelValue.setText(scoreCard.getPlayerScore() + "");
		levelLabelValue.setText(scoreCard.getLevel() + "");
		linesLabelValue.setText(scoreCard.getNumberOfLinesCompleted() + "");

	}

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new GameGUI().setVisible(true);
			}
		});
	}

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JInternalFrame jInternalFrame2;

	private javax.swing.JLabel scoreLabel;

	private javax.swing.JLabel scoreLabelValue;

	private javax.swing.JLabel linesLabel;

	private javax.swing.JLabel linesLabelValue;

	private javax.swing.JLabel levelLabel;

	private javax.swing.JLabel levelLabelValue;

	private javax.swing.JMenu jMenu1;

	private javax.swing.JMenu jMenu2;

	private javax.swing.JMenu jMenu4;

	private javax.swing.JMenuBar jMenuBar1;

	private javax.swing.JMenuItem jMenuItem1;

	private javax.swing.JMenuItem jMenuItem10;

	private javax.swing.JMenuItem jMenuItem11;

	private javax.swing.JMenuItem jMenuItem2;

	private javax.swing.JMenuItem jMenuItem3;

	private javax.swing.JMenuItem jMenuItem4;

	private javax.swing.JMenuItem jMenuItem5;

	private javax.swing.JMenuItem jMenuItem6;

	private javax.swing.JMenuItem jMenuItem7;

	private javax.swing.JMenuItem jMenuItem8;

	private javax.swing.JMenuItem jMenuItem9;

	private javax.swing.JPanel jPanel1;

	private javax.swing.JPanel jPanel2;

	private javax.swing.JPanel scorePanel;

	private javax.swing.JPanel nextShapePanel;

	private MyGamePanel gamePanel;

	private java.awt.Menu menu1;

	private java.awt.MenuBar menuBar1;
	// End of variables declaration//GEN-END:variables

}
