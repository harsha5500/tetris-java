/**
 * 
 */

/**
 * @author Admin
 * 
 */
public class Fall implements Move {
	static Controller controller;

	/**
	 * movement is done after validation
	 */

	public void movement() {
		controller = Controller.getInstance();
		if (verify()) {
			int yPos = controller.getShapeLocation().getY();
			yPos += 1;
			if (yPos > MyGamePanel.YCells)
				yPos = MyGamePanel.YCells;
			controller.getShapeLocation().setY(yPos);
		}
	}

	/**
	 * verifies wheather movable
	 */
	public boolean verify() {
		controller.setShapeBottomEdgePosition();
		// System.out.println("testin..1");
		// Util.displayArray(controller.shapeBottomEdgePosition);
		controller.setShapeFinalPosition();
		// System.out.println("\ntestin..2");
		// Util.displayArray(controller.shapeFinalPosition);

		for (int i = 0; i < Util.getShapeWidth(controller.getCurrentShape()); i++) {
			// System.out.println((controller.shapeBottomEdgePosition[i]+1) +"
			// >= "+ (controller.shapeFinalPosition[i]));
			if ((controller.shapeBottomEdgePosition[i] + 1) >= (controller.shapeFinalPosition[i])) {
				// canvas.updateCanvas(currentShape,controller.getShapeLocation());
				controller.checkAndUpdateCanvas();
				controller.setCurrentShape();

				return false;
			}

		}
		return true;
	}

}