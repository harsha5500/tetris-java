/**
 * This class represents LShape shape and has a predefined shape bit pattern
 */

/**
 * @author Admin
 * 
 */
public class ZShape extends Shape {
	/**
	 *
	 */
	public ZShape() {

		Integer[][] theShapeBitPattern = { { 1, 1, 0, 0 }, { 0, 1, 1, 0 },
				{ 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

		super.setShapeBitPattern(theShapeBitPattern);
	}
}