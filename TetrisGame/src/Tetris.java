import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 * Add one sentence class summary here. Add class description here.
 * 
 * @author SRINIVASA REDDY B.R (2007119)
 * @version 1.0, Apr 2, 2008
 */
public class Tetris extends JFrame {

	private ScoreCard scoreCard;

	private Controller controller;

	private GameSetting gameSetting;

	private KeyConfig keyConfig;

	public ProgressGame t;

	private static int OFF = 0;

	private static int ON = 1;

	private int gameActionListener = Tetris.OFF;
	/**
	 * Launch the application
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			Tetris frame = new Tetris();
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the frame
	 */
	public Tetris() {
		super();
		setTitle("TETRIS");
		getContentPane().setLayout(null);
		setBounds(100, 100, 500, 402);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new LineBorder(Color.black, 1, false));
		panel.setBounds(21, 37, 252, 294);
		getContentPane().add(panel);

		final JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(Color.black, 1, false));
		panel_1.setLayout(null);
		panel_1.setBounds(310, 38, 158, 293);
		getContentPane().add(panel_1);

		final JPanel panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(Color.black, 1, false));
		panel_2.setBounds(17, 14, 118, 88);
		panel_1.add(panel_2);

		final JLabel nextShapeLabel = new JLabel();
		nextShapeLabel.setText("next Shape");
		panel_2.add(nextShapeLabel);

		final JLabel scoreLabel = new JLabel();
		scoreLabel.setText("score :");
		scoreLabel.setBounds(10, 122, 66, 16);
		panel_1.add(scoreLabel);

		final JLabel label = new JLabel();
		label.setText("0");
		label.setBounds(82, 122, 83, 16);
		panel_1.add(label);

		final JLabel scoreLabel_1 = new JLabel();
		scoreLabel_1.setText("lines completed:");
		scoreLabel_1.setBounds(10, 170, 101, 16);
		panel_1.add(scoreLabel_1);

		final JLabel label_1 = new JLabel();
		label_1.setText("0");
		label_1.setBounds(117, 170, 83, 16);
		panel_1.add(label_1);

		final JLabel scoreLabel_1_1 = new JLabel();
		scoreLabel_1_1.setText("level :");
		scoreLabel_1_1.setBounds(10, 219, 66, 16);
		panel_1.add(scoreLabel_1_1);

		final JLabel label_1_1 = new JLabel();
		label_1_1.setText("1");
		label_1_1.setBounds(82, 219, 83, 16);
		panel_1.add(label_1_1);

		final JMenuBar menuBar = new JMenuBar();
		menuBar.setLayout(null);
		setJMenuBar(menuBar);

		final JMenu menu = new JMenu();
		menu.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
			}
		});
		menu.setText("Menu");
		menu.setBounds(0, 0, 120, 30);
		menuBar.add(menu);

		final JMenuItem newGameMenuItem = new JMenuItem();
		newGameMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
				
			}
		});
		newGameMenuItem.setText("New Game");
		menu.add(newGameMenuItem);

		final JMenuItem loadGameMenuItem = new JMenuItem();
		loadGameMenuItem.setText("Load Game");
		menu.add(loadGameMenuItem);

		final JMenuItem saveGameMenuItem = new JMenuItem();
		saveGameMenuItem.setText("Save Game");
		menu.add(saveGameMenuItem);

		final JMenuItem pauseresumeMenuItem = new JMenuItem();
		pauseresumeMenuItem.setText("Pause/Resume");
		menu.add(pauseresumeMenuItem);

		final JMenuItem viewHighScoresMenuItem = new JMenuItem();
		viewHighScoresMenuItem.setText("View High Scores");
		menu.add(viewHighScoresMenuItem);

		final JMenuItem endGameMenuItem = new JMenuItem();
		endGameMenuItem.setText("End Game");
		menu.add(endGameMenuItem);

		final JMenuItem exitGameMenuItem = new JMenuItem();
		exitGameMenuItem.setText("Exit Game");
		menu.add(exitGameMenuItem);

		final JMenu settingsMenu = new JMenu();
		settingsMenu.setText("Settings");
		settingsMenu.setBounds(0, 0, 120, 30);
		menuBar.add(settingsMenu);

		final JMenuItem gameSettingsMenuItem = new JMenuItem();
		gameSettingsMenuItem.setText("Game settings");
		settingsMenu.add(gameSettingsMenuItem);

		final JMenuItem configureControlsMenuItem = new JMenuItem();
		configureControlsMenuItem.setText("Configure controls");
		settingsMenu.add(configureControlsMenuItem);

		final JMenu helpMenu = new JMenu();
		helpMenu.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent arg0) {
			}
		});
		helpMenu.setText("Help");
		helpMenu.setBounds(0, 0, 120, 30);
		menuBar.add(helpMenu);

		final JMenuItem howToPalyMenuItem = new JMenuItem();
		howToPalyMenuItem.setText("How to play");
		helpMenu.add(howToPalyMenuItem);

		final JMenuItem aboutMenuItem = new JMenuItem();
		aboutMenuItem.setText("About");
		helpMenu.add(aboutMenuItem);

		final JMenuItem creditsMenuItem = new JMenuItem();
		creditsMenuItem.setText("Credits");
		helpMenu.add(creditsMenuItem);
		//
	}
	
	

}
