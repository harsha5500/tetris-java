import java.io.IOException;

/**
 * 
 */

/**
 * @author Admin
 * 
 * 
 */
public class GameSetting {
	/**
	 * 
	 * 
	 */

	private static GameSetting gameSetting;

	/**
	 * @return
	 * returns object of singleton GameSetting class
	 * 
	 */
	public static GameSetting getInstance() {
		if (gameSetting == null) {
			gameSetting = new GameSetting();
		}

		return gameSetting;
	}

	private Boolean shadowFlag;

	/**
	 * 
	 * 
	 */
	private Boolean previewFlag;

	/**
	 * 
	 * 
	 */
	private Integer prefilledRows;

	/**
	 * 
	 * 
	 */
	private Integer startLevel;

	/**
	 * constructor being private for singleton
	 * 
	 */
	private GameSetting() {
		// begin-user-code
		// TODO Auto-generated constructor stub
		// end-user-code
	}

	/**
	 * @return the prefilledRows
	 * 
	 * 
	 */
	public Integer getPrefilledRows() {
		// begin-user-code
		return prefilledRows;
		// end-user-code
	}

	/**
	 * @return the previewFlag
	 * 
	 * 
	 */
	public Boolean getPreviewFlag() {
		// begin-user-code
		return previewFlag;
		// end-user-code
	}

	/**
	 * @return the shadowFlag
	 * 
	 * 
	 */
	public Boolean getShadowFlag() {
		// begin-user-code
		return shadowFlag;
		// end-user-code
	}

	/**
	 * @return the startLevel
	 * 
	 * 
	 */
	public Integer getStartLevel() {
		// begin-user-code
		return startLevel;
		// end-user-code
	}

	/**
	 * Read a file and set game settings while start of a game
	 *
	 */
	public void readFileAndSetGameSettings() {
		String filename = "gameSetting.txt";
		String readFromFile = "";

		// read start level first
		try {
			readFromFile = Util.readLine(filename, 0);

			startLevel = Integer.parseInt(readFromFile);

			// read prefilled rows second
			readFromFile = Util.readLine(filename, 1);
			prefilledRows = Integer.parseInt(readFromFile);

			// read Preview flag third
			readFromFile = Util.readLine(filename, 2);
			previewFlag = Boolean.parseBoolean(readFromFile);

			// read shadow flag fourth
			readFromFile = Util.readLine(filename, 3);
			shadowFlag = Boolean.parseBoolean(readFromFile);
		} catch (IOException e) {

			this.startLevel = 1;
			this.prefilledRows = 0;
			this.previewFlag = true;
			this.shadowFlag = true;
		}

	}

	/**
	 * Save the current game settings into a file
	 *
	 */
	public void saveGameSettingsToFile() {
		String fileName = "gameSetting.txt";
		String toWriteToFile = getStartLevel().toString() + "\n"
		+ getPrefilledRows().toString() + "\n"
		+ getPreviewFlag().toString() + "\n"
		+ getShadowFlag().toString();
		try {
			Util.write(fileName, toWriteToFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Sets all the parameters that are passed 
	 * @param thePrefilledRows
	 * @param theStartLevel
	 * @param thePreviewFlag
	 * @param theShadowFlag
	 */
	public void setAllParameters(int thePrefilledRows, int theStartLevel,
			boolean thePreviewFlag, boolean theShadowFlag) {
		setPrefilledRows(thePrefilledRows);
		setStartLevel(theStartLevel);
		setPreviewFlag(thePreviewFlag);
		setShadowFlag(theShadowFlag);

	}

	/**
	 * @param thePrefilledRows
	 *            the prefilledRows to set
	 * 
	 * 
	 */
	public void setPrefilledRows(Integer thePrefilledRows) {
		// begin-user-code
		prefilledRows = thePrefilledRows;
		// end-user-code
	}

	/**
	 * @param thePreviewFlag
	 *            the previewFlag to set
	 * 
	 * 
	 */
	public void setPreviewFlag(Boolean thePreviewFlag) {
		// begin-user-code
		previewFlag = thePreviewFlag;
		// end-user-code
	}

	/**
	 * @param theShadowFlag
	 *            the shadowFlag to set
	 * 
	 * 
	 */
	public void setShadowFlag(Boolean theShadowFlag) {
		// begin-user-code
		shadowFlag = theShadowFlag;
		// end-user-code
	}

	/**
	 * @param theStartLevel
	 *            the startLevel to set
	 * 
	 * 
	 */
	public void setStartLevel(Integer theStartLevel) {
		// begin-user-code
		startLevel = theStartLevel;
		// end-user-code
	}
}