/**
 * This class represents InvertedZShape shape and has a predefined shape bit pattern
*/

/**
 * @author Admin

 *            
 */
public class InvertedZShape extends Shape {
	/**
	 * pre define shape bitvector           
	 */
	public InvertedZShape() {

		Integer[][] theShapeBitPattern = { { 0, 1, 1, 0 }, { 1, 1, 0, 0 },
				{ 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

		super.setShapeBitPattern(theShapeBitPattern);
	}
}