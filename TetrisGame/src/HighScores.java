import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

import javax.swing.JOptionPane;

public class HighScores implements Comparable {

	int playerscore;

	String playerName;

	ArrayList<HighScores> highScore = null;

	public HighScores() {
		highScore = new ArrayList<HighScores>();
	}

	public HighScores(String name, int score) {
		highScore = null;
		this.playerName = name;
		this.playerscore = score;
	}

	public void getScoresFromFile() {
		String filename = "HiScores.txt";
		try {
			BufferedReader inputFile = new BufferedReader(new FileReader(
					filename));

			/* Read all entries from the file */
			for (int i = 0; i < 5; i++)// This will always be five lines
			{
				String scoreDetails[] = inputFile.readLine().split(";");
				/* This will always have 2 elements */
				HighScores temp = new HighScores(scoreDetails[0], Integer
						.parseInt(scoreDetails[1]));
				highScore.add(temp);
			}
			inputFile.close();
			Collections.sort(highScore);
			// debug
			for (int i = 0; i < highScore.size(); i++) {
				HighScores temp = highScore.get(i);
				System.out.println("Scores");
				System.out.println(temp.playerName);
				System.out.println(temp.playerscore);

			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void updateHighScores(Integer currentScore) {
		String myName = "";
		HighScores temp = highScore.get(0);

		/* if current score qualifies for high score */
		if (temp.playerscore < currentScore) {
			// myName = JOptionPane.showInputDialog(null, JOptionPane.OK_OPTION,
			// "Enter name");
			myName = JOptionPane.showInputDialog("Enter name");
			temp = new HighScores(myName, currentScore);
			highScore.add(temp);
			Collections.sort(highScore);
			writeScoresToFile();

			// debug
			System.out.println("Size" + highScore.size());
		}
	}

	public void writeScoresToFile() {
		String filename = "HiScores.txt";
		File f = new File(filename);
		f.delete();
		f = new File(filename);
		try {

			// BufferedWriter outputFile = new BufferedWriter(new FileWriter(new
			// File(filename)));
			System.out.println("here" + highScore.size());
			highScore.remove(highScore.get(0));

			for (int i = 0; i < 5; i++) {
				Util.append(filename, highScore.get(i).playerName + ";"
						+ highScore.get(i).playerscore);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public void displayHighScores() {
		
		getScoresFromFile();
		ScoresGUI scoresGUI = new ScoresGUI(highScore);
	}

	public static void main(String[] args) {
		HighScores t = new HighScores();
		t.getScoresFromFile();
		t.updateHighScores(101);
	}

	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub

		if (this.playerscore > ((HighScores) arg0).playerscore)
			return 1;
		else if (this.playerscore == ((HighScores) arg0).playerscore)
			return 0;
		else
			return -1;
	}

}