/**
 * Utility class consists of various utilities used
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Util {
	// delay 
	public static void delay(int dly) {
		try {
			Thread.sleep(dly);

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// displays a 1-D array
	public static void displayArray(Integer[] arr) {
		for (int i = 0; i < arr.length; i++) {

			System.out.print(arr[i] + "  ");
		}

	}

	// displays a 2-D array
	public static void displayArray(Integer[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println("");

		}
	}

	// gets minimum element of array
	public static int getMinOfArray(Integer[] arr) {
		int min = arr[0];
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] != null && arr[i] < min)
				min = arr[i];
		}
		return min;
	}

	// gets index of minimum element of array
	public static int getIndexOfMinElementOfArray(Integer[] arr) {
		int minIndex = 0;
		for (int i = 1; i < arr.length; i++) {
			if (arr[i] != null && arr[i] < arr[minIndex])
				minIndex = i;
		}
		return minIndex;
	}

	// returns width of a shape
	public static int getShapeWidth(Shape shape) {
		int currentblockWidth = 4;
		Integer[][] shapeBitPattern = shape.getShapeBitPattern();
		// current block width
		for (int i = 0; i < shapeBitPattern.length; i++) {
			if ((shapeBitPattern[0][i] + shapeBitPattern[1][i]
					+ shapeBitPattern[2][i] + shapeBitPattern[3][i]) == 0) {
				currentblockWidth = i;
				break;
			}
		}
		return currentblockWidth;
	}

	// returns height of a shape
	public static int getShapeHeight(Shape shape) {
		int currentblockHeight = 4;
		Integer[][] shapeBitPattern = shape.getShapeBitPattern();
		// current block height
		for (int i = 0; i < shapeBitPattern.length; i++) {
			if ((shapeBitPattern[i][0] + shapeBitPattern[i][1]
					+ shapeBitPattern[i][2] + shapeBitPattern[i][3]) == 0) {
				currentblockHeight = i;
				break;
			}
		}
		// System.out.println(currentblockHeight + " bh");
		return currentblockHeight;
	}

	// returns index of highest column of a shape
	public static int getShapeHeightAtIndex(Shape shape, int index) {
		int shapeHeightAtIndex = 0;
		Integer[][] shapeBitPattern = shape.getShapeBitPattern();

		for (int i = 0; i < shapeBitPattern.length; i++) {
			if (shapeBitPattern[i][index] == 1) {
				shapeHeightAtIndex = (i + 1);
			}
		}
		return shapeHeightAtIndex;

	}

	// returns arraylist of strings read from given filename
	public static ArrayList read(String fileName) throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		String s;
		ArrayList<String> result = new ArrayList<String>();
		while ((s = in.readLine()) != null) {
			result.add(s);
		}
		in.close();
		return result;
	}

	// returns a line  read from filename at given line number
	public static String readLine(String fileName, int lineNum)
			throws IOException {
		StringBuffer sb = new StringBuffer();
		BufferedReader in = new BufferedReader(new FileReader(fileName));
		String s = null;

		int i = 0;
		while (i <= lineNum && (s = in.readLine()) != null) {
			i++;
		}
		in.close();
		return s;
	}

	// writes the text into the file
	public static void write(String fileName, String text) throws IOException {
		File f = new File(fileName);
		f.delete();
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
				fileName)));
		out.println(text);
		out.close();
	}

	// appends the text into the file
	public static void append(String FileName, String text) throws IOException {
		PrintWriter out1 = new PrintWriter(new BufferedWriter(new FileWriter(
				FileName, true)));
		out1.println(text);
		out1.close();
	}

	// serializes the object and stores the object in text form
	static void store(Object myObj, String filename) {
		try {

			FileOutputStream fos = null;
			ObjectOutputStream oos = null;

			System.out.println("storing...");
			fos = new FileOutputStream(filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(myObj);
			oos.close();
			System.out.println("Object Persisted");
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	// De serializes the object and loads the object in text form
	public static Object load(String filename) {
		Object obj = null;
		try {
			FileInputStream fis = null;
			ObjectInputStream ois = null;

			System.out.println("loadin...");
			fis = new FileInputStream(filename);
			ois = new ObjectInputStream(fis);
			obj = ois.readObject();
			ois.close();
			System.out.println("loading sucess");
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		return obj;
	}

	// returns shape bottom position 
	public static Integer[] getShapeBottomPosition(Shape shape) {
		boolean setter = false;
		Integer[] shapeBottomEdgePosition = new Integer[MyGamePanel.blockSizeX];
		Integer[][] shapeBitPattern = shape.getShapeBitPattern();

		for (int j = 0; j < shapeBitPattern[0].length; j++) {
			setter = false;
			for (int i = 0; i < shapeBitPattern.length; i++) {
				if (shapeBitPattern[3 - i][j] == 1) {
					shapeBottomEdgePosition[j] = (3 - i);
					setter = true;
					break;
					// System.out.print(shapeBitPattern[j][i]+" dd ");
				}
			}
			// System.out.println();
			if (setter == false)
				shapeBottomEdgePosition[j] = 0;
			// System.out.println(shapeBottomEdgePosition[j]+" dd ");
		}
		return shapeBottomEdgePosition;

	}

}
