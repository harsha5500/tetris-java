import java.io.Serializable;

import javax.swing.JOptionPane;

/**
 * @author Admin
 * 
 * 
 */
public class Controller implements Serializable {
	// attributes of controller
	private Shape nextShape;

	private static Controller controller;

	public Integer[] shapeBottomEdgePosition;

	public Integer[] shapeFinalPosition;

	/**
	 * @return the nextShape
	 * 
	 * 
	 */
	public Shape getNextShape() {
		// begin-user-code
		return nextShape;
		// end-user-code
	}

	/**
	 * @param theNextShape
	 *            the nextShape to set
	 * 
	 * 
	 */
	public void setNextShape(Shape theNextShape) {
		// begin-user-code
		nextShape = theNextShape;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Shape currentShape;

	/**
	 * @return the currentShape
	 * 
	 * 
	 */
	public Shape getCurrentShape() {
		// begin-user-code
		return currentShape;
		// end-user-code
	}

	/**
	 * @param theCurrentShape
	 *            the currentShape to set
	 * 
	 * 
	 */
	public void setCurrentShape(Shape theCurrentShape) {
		// begin-user-code
		currentShape = theCurrentShape;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Location shapeLocation;

	/**
	 * @return the shapeLocation
	 * 
	 * 
	 */
	public Location getShapeLocation() {
		// begin-user-code
		return shapeLocation;
		// end-user-code
	}

	/**
	 * @param theShapeLocation
	 *            the shapeLocation to set
	 * 
	 * 
	 */
	public void setShapeLocation(Location theShapeLocation) {
		// begin-user-code
		shapeLocation.setX(theShapeLocation.getX());
		shapeLocation.setY(theShapeLocation.getY());
		// end-user-code
	}

	public void setShapeLocation2(int x, int y) {
		// begin-user-code
		shapeLocation.setX(x);
		shapeLocation.setY(y);
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private int speed;

	/**
	 * @return the speed
	 * 
	 * 
	 */
	public int getSpeed() {
		// begin-user-code
		return speed;
		// end-user-code
	}

	/**
	 * @param theSpeed
	 *            the speed to set
	 * 
	 * 
	 */
	public void setSpeed(int theSpeed) {
		// begin-user-code
		speed = theSpeed;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	Canvas canvas;

	/**
	 * @return the canvas
	 * 
	 * 
	 */
	public Canvas getCanvas() {
		// begin-user-code
		return canvas;
		// end-user-code
	}

	/**
	 * @param theCanvas
	 *            the canvas to set
	 * 
	 * 
	 */
	public void setCanvas(Canvas theCanvas) {
		// begin-user-code
		canvas = theCanvas;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Move move;

	/**
	 * @return the move
	 * 
	 * 
	 */
	public Move getMove() {
		// begin-user-code
		return move;
		// end-user-code
	}

	/**
	 * @param theMove
	 *            the move to set
	 * 
	 * 
	 */
	public void setMove(Move theMove) {
		// begin-user-code
		move = theMove;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Integer currentHighestLine;

	/**
	 * @return the currentHighestLine
	 * 
	 * 
	 */
	public Integer getCurrentHighestLine() {
		// begin-user-code
		return currentHighestLine;
		// end-user-code
	}

	/**
	 * @param theCurrentHighestLine
	 *            the currentHighestLine to set
	 * 
	 * 
	 */
	public void setCurrentHighestLine() {
		Integer[][] stateVector = ((GameCanvas) controller.getCanvas())
				.getStateVector();
		for (int i = 0; i < stateVector.length; i++) {
			for (int j = 0; j < stateVector[0].length; j++) {
				if (stateVector[i][j] != 0) {
					currentHighestLine = i;
					// System.out.println(" @ cntlr"+currentHighestLine);
					return;
				}

			}
			/*
			 * if(done == true) break;
			 */
		}
	}

	/**
	 * 
	 * 
	 */
	private Integer topLine;

	/**
	 * @return the topLine
	 * 
	 * 
	 */
	public Integer getTopLine() {
		// begin-user-code
		return topLine;
		// end-user-code
	}

	/**
	 * @param theTopLine
	 *            the topLine to set
	 * 
	 * 
	 */
	public void setTopLine(Integer theTopLine) {
		// begin-user-code
		topLine = theTopLine;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Boolean previewFlag;

	/**
	 * @return the previewFlag
	 * 
	 * 
	 */
	public Boolean getPreviewFlag() {
		// begin-user-code
		return previewFlag;
		// end-user-code
	}

	/**
	 * @param thePreviewFlag
	 *            the previewFlag to set
	 * 
	 * 
	 */
	public void setPreviewFlag(Boolean thePreviewFlag) {
		// begin-user-code
		previewFlag = thePreviewFlag;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Boolean shadowFlag;

	/**
	 * @return the shadowFlag
	 * 
	 * 
	 */
	public Boolean getShadowFlag() {
		// begin-user-code
		return shadowFlag;
		// end-user-code
	}

	/**
	 * @param theShadowFlag
	 *            the shadowFlag to set
	 * 
	 * 
	 */
	public void setShadowFlag(Boolean theShadowFlag) {
		// begin-user-code
		shadowFlag = theShadowFlag;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Integer pauseResumeEndFlag;

	/**
	 * @return the pauseResumeEndFlag
	 * 
	 * 
	 */
	public Integer getPauseResumeEndFlag() {
		// begin-user-code
		return pauseResumeEndFlag;
		// end-user-code
	}

	/**
	 * @param thePauseResumeEndFlag
	 *            the pauseResumeEndFlag to set
	 * 
	 * 
	 */
	public void setPauseResumeEndFlag(Integer thePauseResumeEndFlag) {
		// begin-user-code
		pauseResumeEndFlag = thePauseResumeEndFlag;
		// end-user-code
	}

	/**
	 * 
	 * 
	 */
	private Controller() {
		// begin-user-code
		// TODO Auto-generated constructor stub
		// end-user-code
	}

	/**
	 * @return instance of controller
	 * 
	 * 
	 */
	public static Controller getInstance() {
		if (controller == null) {
			controller = new Controller();
		}

		return controller;
	}

	/**
	 * @param level
	 *            initilize the controller with a start level
	 * 
	 */
	public void initializeController(Integer level) {
		nextShape = ShapeGenerator.generateShape();
		currentShape = ShapeGenerator.generateShape();
		shapeLocation = new Location(6, 0);
		shapeBottomEdgePosition = new Integer[MyGamePanel.blockSizeX];
		shapeFinalPosition = new Integer[MyGamePanel.blockSizeX];
		speed = (1 / level) * 1000;
		canvas = GameCanvas.getInstance();
		canvas.initialize(GameSetting.getInstance().getPrefilledRows());
		currentHighestLine = 20;
		topLine = 0;
		previewFlag = GameSetting.getInstance().getPreviewFlag();
		shadowFlag = GameSetting.getInstance().getShadowFlag();
		pauseResumeEndFlag = 2;
	}

	/**
	 * @param controller
	 *            initialize the controller with a controller object
	 * 
	 */
	public void initializeController(Controller controller) {
		Controller.controller = controller;
	}

	/**
	 * updates the canvas with the current shape
	 * 
	 */
	public void checkAndUpdateCanvas() {

		canvas.updateCanvas(currentShape, controller.getShapeLocation());

	}

	/**
	 * sets the nexxt shape to current shape and gets a new next shape from
	 * shape factory
	 * 
	 */
	public void setCurrentShape() {
		setCurrentShape(nextShape);
		shapeLocation = new Location(6, 0);
		speed = 1 / (ScoreCard.getInstance().getLevel());
		setNextShape(ShapeGenerator.generateShape());
		
		/*CHANGE 3 see the shape generated and update the score along with the current score*/
		if(currentShape.getClass().getName().equals("BoxShape"))
			ScoreCard.getInstance().updateScoreCard(2);
		else if(currentShape.getClass().getName().equals("LShape"))
			ScoreCard.getInstance().updateScoreCard(3);
		else if(currentShape.getClass().getName().equals("InvertedLShape"))
			ScoreCard.getInstance().updateScoreCard(3);
		else if(currentShape.getClass().getName().equals("InvertedZShape"))
			ScoreCard.getInstance().updateScoreCard(5);
		else if(currentShape.getClass().getName().equals("ZShape"))
			ScoreCard.getInstance().updateScoreCard(5);
		else
			ScoreCard.getInstance().updateScoreCard(1);
	}

	/**
	 * checks wheather the game has end or not
	 * 
	 */
	public void checkEndGame() {
		setCurrentHighestLine();
		// System.out.println("top line "+currentHighestLine+" "+topLine);
		if (currentHighestLine == topLine)
			pauseResumeEndFlag = 3;
	}

	/**
	 * checks the canvas state vector to determine completed lines in any
	 * 
	 */
	public void checkCompletedLines() {
		boolean lineCompleted = true;
		Integer[][] stateVector = ((GameCanvas) controller.getCanvas())
				.getStateVector();
		for (int i = 0; i < stateVector.length; i++) {
			lineCompleted = true;
			for (int j = 0; j < stateVector[0].length; j++) {
				if (stateVector[i][j] != 1) {
					lineCompleted = false;
					break;
				}
			}
			if (lineCompleted == true) {

				// System.out.println("remove"+ i+" "+currentHighestLine);
				((GameCanvas) canvas)
						.removeCompletedLine(i, currentHighestLine);
				ScoreCard.getInstance().updateScoreCard(1);
				i = 0;
				currentHighestLine++;
			}
		}

	}

	/**
	 * Ends the current game
	 * 
	 */
	public void endGame() {
		JOptionPane.showMessageDialog(null, "Game Over ...");
		// System.exit(0);
	}

	/**
	 * gets the shape bottom edge position w.r.t canvas state vector
	 * 
	 */
	public void setShapeBottomEdgePosition() {
		boolean setter = false;
		Integer[][] shapeBitPattern = controller.getCurrentShape()
				.getShapeBitPattern();
		for (int j = 0; j < shapeBitPattern.length; j++) {
			setter = false;
			for (int i = 0; i < shapeBitPattern.length; i++) {
				if (shapeBitPattern[3 - i][j] == 1) {
					shapeBottomEdgePosition[j] = (3 - i)
							+ controller.getShapeLocation().getY();
					setter = true;
					break;
					// System.out.print(shapeBitPattern[j][i]+" dd ");
				}
			}
			// System.out.println();
			if (setter == false)
				shapeBottomEdgePosition[j] = 0;
			// System.out.println(shapeBottomEdgePosition[j]+" dd ");
		}
		// System.out.println();

	}

	/**
	 * Gets the shape final position underneath the current shape at a shape
	 * location
	 * 
	 */
	public void setShapeFinalPosition() {
		boolean setter = false;
		Integer[][] stateVector = ((GameCanvas) controller.getCanvas())
				.getStateVector();
		int x = this.getShapeLocation().getX();

		for (int j = x; j < x
				+ Util.getShapeWidth(controller.getCurrentShape()); j++) {
			setter = false;
			for (int i = 0; i < MyGamePanel.YCells; i++) {
				// System.out.println("i j "+i+" "+j);
				if (stateVector[i][j] == 1) {
					shapeFinalPosition[j - x] = (i);
					setter = true;
					break;
					// System.out.print(shapeBitPattern[j][i]+" ");
				}
			}
			// System.out.println();
			if (setter == false)
				shapeFinalPosition[j - x] = 20;
			// System.out.println(shapeFinalPosition[j-x]+" cc ");
		}

	}

	public boolean checkDropable() {
		return false;

	}

}