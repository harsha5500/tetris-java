/**Interface defines all the methods that a move objects should have
 * 
 */

/**
 * @author Admin
 * 
 */
public interface Move {
	/**
	 * method called to move only after verification
	 */
	public void movement();

	/**
	 * vewrifies the movement
	 */
	public boolean verify();
}