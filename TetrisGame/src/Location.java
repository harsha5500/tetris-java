import java.io.Serializable;

/**
 * 
 */

/**
 * @author Admin
 * 
 */
public class Location implements Serializable {
	/**
	 * 
	 */
	private Integer x;

	/**
	 * @return the x position
	 *           
	 */
	public Integer getX() {
		// begin-user-code
		return x;
		// end-user-code
	}

	/**
	 * @param theX
	 *            the new x location  to set

	 *            
	 */
	public void setX(Integer theX) {
		// begin-user-code
		x = theX;
		// end-user-code
	}

	/**

	 *            
	 */
	private Integer y;

	/**
	 * @return the y position

	 *            
	 */
	public Integer getY() {
		// begin-user-code
		return y;
		// end-user-code
	}

	/**
	 * @param theY
	 *            the new y location  to set

	 *            
	 */
	public void setY(Integer theY) {
		// begin-user-code
		y = theY;
		// end-user-code
	}

	/**
	 * @param x
	 * @param y

	 *            
	 */
	public Location(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}
}