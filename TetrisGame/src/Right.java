/**
 *  Class for move object to move current shape to right on right key action
 */

/**
 * @author Admin
 * @generated "UML to Java V5.0
 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
 */
public class Right implements Move {
	static Controller controller;

	/**
	 *  method to change x shape location by 1 unit 
	 */

	public void movement() {
		controller = Controller.getInstance();
		int xPos = controller.getShapeLocation().getX();
		if (verify()) {
			xPos += 1;
			if (xPos > MyGamePanel.XCells)
				xPos = MyGamePanel.XCells;
			controller.getShapeLocation().setX(xPos);
		}
	}

	/**
	 * verify movable to right
	 */
	public boolean verify() {
		Integer[][] canvasStateVector = ((GameCanvas) controller.getCanvas())
				.getStateVector();
		Integer[][] shapeBitPattern = controller.getCurrentShape()
				.getShapeBitPattern();
		int xPos = controller.getShapeLocation().getX();
		int yPos = controller.getShapeLocation().getY();
		int currentblockWidth = Util
				.getShapeWidth(controller.getCurrentShape());
		if (xPos >= (MyGamePanel.XCells - currentblockWidth))
			return false;

		// System.out.println(currentblockWidth+" bw");

		for (int i = 0; i < MyGamePanel.blockSizeY; i++) {
			// System.out.println((controller.shapeBottomEdgePosition[i]+1) +"
			// >= "+ (controller.shapeFinalPosition[i]));
			if (shapeBitPattern[i][currentblockWidth - 1] == 1
					&& canvasStateVector[yPos + i][xPos + currentblockWidth] == 1)

				return false;
		}

		return true;
	}

}