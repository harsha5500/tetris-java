import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Hashtable;

/**
 * 
 */

/**
 * @author Admin
 
 *           
 */
public class KeyConfig {
	private static KeyConfig keyConfig;

	/**
	 * @return
	 * returns object of the singleton class KeyConfig	 
	 *           
	 */
	public static KeyConfig getInstance() {

		if (keyConfig == null) {
			keyConfig = new KeyConfig();
		}

		return keyConfig;
	}

	
	private Integer Rotate;

	private Integer Drop;

	private Integer Down;

	private Integer Right;

	private Integer Left;

	public Hashtable<String, Integer> keyMap = null;

	/**
	 * Setup the hash map which maps the seyyings to keycode
	 * constructor
	 */
	private KeyConfig() {
		keyMap = new Hashtable<String, Integer>();

		keyMap.put("Left", KeyEvent.VK_LEFT);
		keyMap.put("Right", KeyEvent.VK_RIGHT);
		keyMap.put("Up", KeyEvent.VK_UP);
		keyMap.put("Down", KeyEvent.VK_DOWN);

		keyMap.put("Ctrl", KeyEvent.VK_CONTROL);
		keyMap.put("Shift", KeyEvent.VK_SHIFT);
		keyMap.put("Alt", KeyEvent.VK_ALT);
		keyMap.put("Enter", KeyEvent.VK_ENTER);

		keyMap.put("A", KeyEvent.VK_A);
		keyMap.put("B", KeyEvent.VK_B);
		keyMap.put("C", KeyEvent.VK_C);
		keyMap.put("D", KeyEvent.VK_D);
		keyMap.put("E", KeyEvent.VK_E);
		keyMap.put("F", KeyEvent.VK_F);

		keyMap.put("G", KeyEvent.VK_G);
		keyMap.put("H", KeyEvent.VK_H);
		keyMap.put("I", KeyEvent.VK_I);
		keyMap.put("J", KeyEvent.VK_J);
		keyMap.put("K", KeyEvent.VK_K);
		keyMap.put("L", KeyEvent.VK_L);

		keyMap.put("M", KeyEvent.VK_M);
		keyMap.put("N", KeyEvent.VK_N);
		keyMap.put("O", KeyEvent.VK_O);
		keyMap.put("P", KeyEvent.VK_P);
		keyMap.put("Q", KeyEvent.VK_Q);
		keyMap.put("R", KeyEvent.VK_R);

		keyMap.put("S", KeyEvent.VK_S);
		keyMap.put("T", KeyEvent.VK_T);
		keyMap.put("U", KeyEvent.VK_U);
		keyMap.put("V", KeyEvent.VK_V);
		keyMap.put("W", KeyEvent.VK_W);
		keyMap.put("X", KeyEvent.VK_X);

		keyMap.put("Y", KeyEvent.VK_Y);
		keyMap.put("Z", KeyEvent.VK_Z);

		keyMap.put("0", KeyEvent.VK_0);
		keyMap.put("1", KeyEvent.VK_1);
		keyMap.put("2", KeyEvent.VK_2);
		keyMap.put("3", KeyEvent.VK_3);
		keyMap.put("4", KeyEvent.VK_4);

		keyMap.put("5", KeyEvent.VK_5);
		keyMap.put("6", KeyEvent.VK_6);
		keyMap.put("7", KeyEvent.VK_7);
		keyMap.put("8", KeyEvent.VK_8);
		keyMap.put("9", KeyEvent.VK_9);

		
	}

	public Integer getDown() {
		// begin-user-code
		return Down;
		// end-user-code
	}

	/**
	 * @return the Drop
	 *           
	 */
	public Integer getDrop() {
		// begin-user-code
		return Drop;
		// end-user-code
	}

	/**
	 * @return the Left	 
	 *           
	 */
	public Integer getLeft() {
		// begin-user-code
		return Left;
		// end-user-code
	}

	/**
	 * @param keyPressed
	 * @return	 
	 *           
	 */
	public Integer getPressedKey(Integer keyPressed) {
		// begin-user-code
		// TODO Auto-generated method stub
		return null;
		// end-user-code
	}

	/**
	 * @return the Right	 
	 *           
	 */
	public Integer getRight() {
		// begin-user-code
		return Right;
		// end-user-code
	}

	/**
	 * @return the Rotate
	 
	 *           
	 */
	public Integer getRotate() {
		// begin-user-code
		return Rotate;
		// end-user-code
	}

	/**
	 *  Read a file and set key settings of KeyConfig
	 *
	 */
	public void readFileAndSetKeyConfig() {
		// begin-user-code
		// TODO Auto-generated method stub
		String fileName = "keyConfig.txt";

		String readLine = "";
		try {

			// read Rotate first
			readLine = Util.readLine(fileName, 0);
			setLeft(Integer.parseInt(readLine));

			// read Left second
			readLine = Util.readLine(fileName, 1);
			setRight(Integer.parseInt(readLine));

			// read Right
			readLine = Util.readLine(fileName, 2);
			setRotate(Integer.parseInt(readLine));

			// Read Down

			readLine = Util.readLine(fileName, 3);
			setDrop(Integer.parseInt(readLine));

		} catch (Exception e) {
			setLeft(KeyEvent.VK_LEFT);
			setRight(KeyEvent.VK_RIGHT);
			setRotate(KeyEvent.VK_UP);
			setDrop(KeyEvent.VK_CONTROL);
		}
		// end-user-code
	}

	/**
	 * Saves the current key settings into a file
	 *           
	 */
	public void saveKeyConfigToFile() {
		// begin-user-code
		// TODO Auto-generated method stub
		String fileName = "keyConfig.txt";

		String toWriteToFile = getLeft().toString() + "\n"
				+ getRight().toString() + "\n" + getRotate().toString() + "\n"
				+ getDrop() + "\n";
		try {
			Util.write(fileName, toWriteToFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// end-user-code
	}

	/**
	 * Sets all the parameters of KeyConfig
	 * @param left
	 * @param right
	 * @param rotate
	 * @param drop
	 */
	public void setAllParameters(int left, int right, int rotate, int drop) {
		setLeft(left);
		setRight(right);
		setRotate(rotate);
		setDrop(drop);

	}

	/**
	 * @param theDown
	 *            the Down to set
	 
	 *           
	 */
	public void setDown(Integer theDown) {
		// begin-user-code
		Down = theDown;
		// end-user-code
	}

	/**
	 * @param theDrop
	 *            the Drop to set
	 
	 *           
	 */
	public void setDrop(Integer theDrop) {
		// begin-user-code
		Drop = theDrop;
		// end-user-code
	}

	/**
	 * @param theLeft
	 *            the Left to set
	 
	 *           
	 */
	public void setLeft(Integer theLeft) {
		// begin-user-code
		Left = theLeft;
		// end-user-code
	}

	/**
	 * @param theRight
	 *            the Right to set
	 
	 *           
	 */
	public void setRight(Integer theRight) {
		// begin-user-code
		Right = theRight;
		// end-user-code
	}

	/**
	 * @param theRotate
	 *            the Rotate to set
	 
	 *           
	 */
	public void setRotate(Integer theRotate) {
		// begin-user-code
		Rotate = theRotate;
		// end-user-code
	}

	public static void main(String[] args) {
		KeyConfig con = KeyConfig.getInstance();
		con.saveKeyConfigToFile();
		con.readFileAndSetKeyConfig();
		System.out.println("dwn " + con.getDown());
	}
}