import java.io.IOException;

/**
 * Class used to maintain the scores of the game 
 */

/**
 * @author Admin
 * @generated "UML to Java V5.0
 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
 */
public class ScoreCard {
	private static ScoreCard scoreCard;

	private GameGUI gameGUI;

	/**
	 * @return
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public static ScoreCard getInstance() {

		if (scoreCard == null) {
			scoreCard = new ScoreCard();
		}

		return scoreCard;

	}

	private static int LEVEL_SCORE_MULTIPLIER = 10;

	private static int NO_OF_LINES_PER_LEVEL = 10;

	/**
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Integer playerScore = 0;

	/**
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Integer numberOfLinesCompleted = 0;

	/**
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Integer level = 0;

	/**
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private ScoreCard() {
		// begin-user-code
		// TODO Auto-generated constructor stub
		// end-user-code
	}

	/**
	 * @return the level
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Integer getLevel() {
		// begin-user-code
		return level;
		// end-user-code
	}

	/**
	 * @return the numberOfLinesCompleted
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Integer getNumberOfLinesCompleted() {
		// begin-user-code
		return numberOfLinesCompleted;
		// end-user-code
	}

	/**
	 * @return the playerScore
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Integer getPlayerScore() {
		// begin-user-code
		return playerScore;
		// end-user-code
	}

	/**
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void initializeScoreCard(int level) {
		this.level = level;
		this.numberOfLinesCompleted = 0;
		this.playerScore = 0;
	}

	/**
	 * @param score
	 * @param noOfLines
	 * @param level
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void initializeScoreCard() {
		this.readFileAndSetKeyConfig();
	}

	/**
	 * @param theLevel
	 *            the level to set
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setLevel(Integer theLevel) {
		// begin-user-code
		level = theLevel;
		// end-user-code
	}

	/**
	 * @param theNumberOfLinesCompleted
	 *            the numberOfLinesCompleted to set
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setNumberOfLinesCompleted(Integer theNumberOfLinesCompleted) {
		// begin-user-code
		numberOfLinesCompleted = theNumberOfLinesCompleted;
		// end-user-code
	}

	/**
	 * @param thePlayerScore
	 *            the playerScore to set
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setPlayerScore(Integer thePlayerScore) {
		// begin-user-code
		playerScore = thePlayerScore;
		// end-user-code
	}

	/**
	 * @param noOfLinesCompleted
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void updateScoreCard(Integer noOfLinesCompleted) {
		this.numberOfLinesCompleted += noOfLinesCompleted;
		this.playerScore += noOfLinesCompleted * level * LEVEL_SCORE_MULTIPLIER;
		checkLevelComplete();
		gameGUI.updateScoreBoard();
	}

	/**
	 * 
	 */
	private void checkLevelComplete() {
		if (numberOfLinesCompleted > level * NO_OF_LINES_PER_LEVEL) {

			level++;

		}

	}

	public void readFileAndSetKeyConfig() {
		// begin-user-code
		// TODO Auto-generated method stub
		String fileName = "scorecard.txt";

		String readLine = "";
		try {

			// read Rotate first
			readLine = Util.readLine(fileName, 0);
			setPlayerScore(Integer.parseInt(readLine));

			// read Left second
			readLine = Util.readLine(fileName, 1);
			setNumberOfLinesCompleted(Integer.parseInt(readLine));

			// read Right
			readLine = Util.readLine(fileName, 2);
			setLevel(Integer.parseInt(readLine));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// end-user-code
	}

	/**
	 * save the game score card into a file while saving a file
	 * 
	 */
	public void saveKeyConfigToFile() {
		// begin-user-code
		// TODO Auto-generated method stub
		String fileName = "scorecard.txt";

		String toWriteToFile = getPlayerScore().toString() + "\n"
				+ getNumberOfLinesCompleted().toString() + "\n"
				+ getLevel().toString() + "\n";

		try {
			Util.write(fileName, toWriteToFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// end-user-code
	}

	/**
	 *store the reference of the game so that score card can be updated 
	 */
	public void setGameGUI(GameGUI gameGUI) {
		this.gameGUI = gameGUI;

	}
}