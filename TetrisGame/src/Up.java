/**
 * 
 */

/**
 * @author Admin
 * @generated "UML to Java V5.0
 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
 */
public class Up implements Move {
	static Controller controller;

	/**
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */

	public void movement() {
		controller = Controller.getInstance();
		int yPos = controller.getShapeLocation().getY();
		if (verify()) {
			yPos -= 1;
			if (yPos < 0)
				yPos = 0;
			controller.getShapeLocation().setY(yPos);
		}
	}

	/**
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public boolean verify() {
		return true;
	}

}