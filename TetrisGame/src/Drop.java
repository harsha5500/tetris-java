/** This class represents drop movement
 * 
 */

/**
 * @author Admin
 * 
 */
public class Drop implements Move {
	static Controller controller;

	/**
	 * movement is done after validation
	 */

	public void movement() {
		controller = Controller.getInstance();
		Move m = new Fall();

		for (int i = 0; i < MyGamePanel.YCells; i++) {
			if (m.verify())
				m.movement();
			else
				break;
		}
	}

	/**
	 * verify droppable which is always true
	 */
	public boolean verify() {

		return true;
	}

}