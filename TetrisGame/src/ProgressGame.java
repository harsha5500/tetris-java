/**
 * This Class is used to create a thread that is used to progress the game.
 * 
 * @author SRINIVASA REDDY B.R (2007119)
 * @version 1.0, Apr 12, 2008
 */
public class ProgressGame implements Runnable {

	Controller controller;

	MyGamePanel panel;

	Thread t;
	
	HighScores highScores = new HighScores();

	/**
	 * constructor
	 */
	public ProgressGame(MyGamePanel panel) {
		controller = Controller.getInstance();
		this.panel = panel;
		t = new Thread(this, "progress game");
		t.start();
	}

	/**
	 * Automatically called upon thread creation
	 */
	public void run() {

		// TestPaint p = new TestPaint(((GameCanvas)canvas).getStateVector());
		// System.out.println("progress game");
		while (true) {

			switch (controller.getPauseResumeEndFlag()) {

			case 1:
				continue;
			case 2:
				//get the current game speed for the delay 
				int speed = (1000 / ScoreCard.getInstance().getLevel());

				Util.delay(speed);
				// if the game status is resume, free fall the current shape
				if (controller.getPauseResumeEndFlag() == 2)
					new Fall().movement();

				// check for end game or completed lines and end game
				controller.checkEndGame();
				controller.checkCompletedLines();
				// repaint the game panel
				panel.repaint();
				break;

			case 3: {
				
				highScores.getScoresFromFile();
				highScores.updateHighScores(ScoreCard.getInstance().getPlayerScore());
				controller.endGame();
				t.stop();
				// stop the thread on game end
				
				
			}
			}
		}

	}

}
