public class Rotate {

	// roatte a shape clockwise once
	public void rotate(Shape shape) {
		Integer[][] pattern = shape.getShapeBitPattern();
		Integer[][] newPattern = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
				{ 0, 0, 0, 0 }, { 0, 0, 0, 0 } };
		newPattern[3][0] = pattern[0][0];
		newPattern[2][0] = pattern[0][1];
		newPattern[1][0] = pattern[0][2];
		newPattern[0][0] = pattern[0][3];
		newPattern[3][1] = pattern[1][0];
		newPattern[2][1] = pattern[1][1];
		newPattern[1][1] = pattern[1][2];
		newPattern[0][1] = pattern[1][3];
		newPattern[3][2] = pattern[2][0];
		newPattern[2][2] = pattern[2][1];
		newPattern[1][2] = pattern[2][2];
		newPattern[0][2] = pattern[2][3];
		newPattern[3][3] = pattern[3][0];
		newPattern[2][3] = pattern[3][1];
		newPattern[1][3] = pattern[3][2];
		newPattern[0][3] = pattern[3][3];

		while (true) {
			if (newPattern[0][0] == 1 || newPattern[0][1] == 1
					|| newPattern[0][2] == 1 || newPattern[0][3] == 1)
				break;
			newPattern[0][0] = newPattern[1][0];
			newPattern[0][1] = newPattern[1][1];
			newPattern[0][2] = newPattern[1][2];
			newPattern[0][3] = newPattern[1][3];
			newPattern[1][0] = newPattern[2][0];
			newPattern[1][1] = newPattern[2][1];
			newPattern[1][2] = newPattern[2][2];
			newPattern[1][3] = newPattern[2][3];
			newPattern[2][0] = newPattern[3][0];
			newPattern[2][1] = newPattern[3][1];
			newPattern[2][2] = newPattern[3][2];
			newPattern[2][3] = newPattern[3][3];
			newPattern[3][0] = 0;
			newPattern[3][1] = 0;
			newPattern[3][2] = 0;
			newPattern[3][3] = 0;
		}

		while (true) {
			if (newPattern[0][0] == 1 || newPattern[1][0] == 1
					|| newPattern[2][0] == 1 || newPattern[3][0] == 1)
				break;
			newPattern[0][0] = newPattern[0][1];
			newPattern[1][0] = newPattern[1][1];
			newPattern[2][0] = newPattern[2][1];
			newPattern[3][0] = newPattern[3][1];
			newPattern[0][1] = newPattern[1][2];
			newPattern[1][1] = newPattern[1][2];
			newPattern[2][1] = newPattern[2][2];
			newPattern[3][1] = newPattern[3][2];
			newPattern[0][2] = newPattern[0][3];
			newPattern[1][2] = newPattern[1][3];
			newPattern[2][2] = newPattern[2][3];
			newPattern[3][2] = newPattern[3][3];
			newPattern[0][3] = 0;
			newPattern[1][3] = 0;
			newPattern[2][3] = 0;
			newPattern[3][3] = 0;
		}

		shape.setShapeBitPattern(newPattern);

	}

	// rotate a shape n times
	public void rotateNTimes(Shape shape, Integer n) {

		for (int i = 0; i < n; i++) {
			rotate(shape);
		}

	}
	
	public boolean verify(){
		
		return false;
	}

}
