/**  Interface for game canvas.
 * 
 */

/**
 * @author Admin
 * 
 */
public interface Canvas {
	/**
	 * @param canvas
	 *            initializes canvas
	 */

	public void initialize(Canvas canvas);

	/**
	 * paints the canvas
	 */
	public void paint();

	/**
	 * clears the state vector of canvas
	 */
	public void clear();

	/**
	 * initializes the canvas with random values for number of prefilled rows
	 */
	public void initialize(Integer prefilledRows);

	/**
	 * updates canvas state vector with a shape bit pattern
	 */
	public void updateCanvas(Shape currentShape, Location shapeLocation);

}