/**
 *  Creates a new shape generating random shape Id and rotation Id and uses a shape factory
 */

import java.util.Random;

/**
 * @author Admin
 * @generated "UML to Java V5.0
 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
 */
public class ShapeGenerator {
	static int NO_OF_SHAPES = 7;

	/**
	 * @return
	 * @generated "UML to Java V5.0
	 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	static Shape shape;

	static int shapeID;

	static int rotationID;

	public static Shape generateShape() {
		Random randomNoGenerator1 = new Random();
		Random randomNoGenerator2 = new Random();

		shapeID = randomNoGenerator1.nextInt(7);

		rotationID = randomNoGenerator2.nextInt(4);

		// System.out.println(" @ sg "+shapeID+ " "+ rotationID);
		shape = ShapeFactory.generateShape(shapeID, rotationID);
		return shape;

	}

}