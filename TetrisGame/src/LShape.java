/**
 * This class represents LShape shape and has a predefined shape bit pattern
*/


/**
 * @author Admin
 * 
 */
public class LShape extends Shape {
	/**
	 *pre define shape bit pattern
	 */
	public LShape() {

		Integer[][] theShapeBitPattern = { { 1, 0, 0, 0 }, { 1, 0, 0, 0 },
				{ 1, 1, 0, 0 }, { 0, 0, 0, 0 } };

		super.setShapeBitPattern(theShapeBitPattern);
	}
}