/**
 *  Represents the game canvas which has a state vector 
 */

import java.io.Serializable;
import java.util.Random;

/**
 * @author Admin
 * 
 * 
 */
public class GameCanvas implements Canvas, Serializable {

	private Integer stateVector[][];

	private static GameCanvas canvas;

	/**
	 * @return the stateVector
	 * 
	 * 
	 */
	public Integer[][] getStateVector() {

		return stateVector;

	}

	/**
	 * @param theStateVector
	 *            the stateVector to set
	 * 
	 * 
	 */
	public void setStateVector(Integer[][] theStateVector) {

		stateVector = theStateVector;

	}

	/**
	 * @param noOfPrefiledRows
	 *            initializes the state vector with a randomly distributed
	 *            prefilled rows
	 * 
	 */

	public void initialize(Integer prefilledRows) {

		Random myGen = new Random();
		Integer[][] theStateVector = new Integer[21][15];
		for (int i = 0; i < theStateVector.length; i++) {
			for (int j = 0; j < theStateVector[0].length; j++) {
				// if (i != theStateVector.length - 1)
				theStateVector[i][j] = 0;

			}
		}
		// else
		// theStateVector[i][j] = 1;

		for (int i = theStateVector.length - prefilledRows; i < theStateVector.length; i++) {
			// for (int j = 0; j < theStateVector[0].length; j++) {
			// if (i != theStateVector.length - 1)
			// theStateVector[i][j] = 0;

			int noOfrandomCells = myGen.nextInt(6);

			for (int k = 0; k < noOfrandomCells; k++) {
				int randomCol = myGen.nextInt(13);
				theStateVector[i][randomCol] = 1;
			}
			// }
			// theStateVector[19][13] = 0;
		}
		this.setStateVector(theStateVector);

	}

	/**
	 * @param canvas
	 *            initilalizes the canvas with the canvas
	 * 
	 */
	public void initialize(Canvas canvas) {

		// TODO Auto-generated method stub

	}

	/**
	 * updates the canvas
	 * 
	 */
	public void update() {

		// TODO Auto-generated method stub

	}

	/**
	 * paints the canvas
	 * 
	 */
	public void paint() {

		// TODO Auto-generated method stub

	}

	/**
	 * clears the canvas
	 * 
	 */
	public void clear() {

		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * @return
	 * the instance of the GameCanvas type singelton class
	 */
	public static GameCanvas getInstance() {
		if (canvas == null) {
			canvas = new GameCanvas();
		}

		return canvas;
	}

	/**
	 * updates the canvas state with the current shape at shape location
	 */
	public void updateCanvas(Shape currentShape, Location shapeLocation) {
		int x = shapeLocation.getX();
		int y = shapeLocation.getY();
		Integer[][] shapeBitPattern = currentShape.getShapeBitPattern();

		for (int i = 0; i < MyGamePanel.blockSizeX; i++) {
			for (int j = 0; j < MyGamePanel.blockSizeY; j++) {
				if (shapeBitPattern[i][j] != 0)
					stateVector[i + y][j + x] = 1;

			}
		}

	}

	/**
	 * removes completed lines from the state vector  of canvas
	 */
	public void removeCompletedLine(int lineToRemove, int currentHighestLine) {

		// shift canvas down by one row
		for (int j = lineToRemove; j > currentHighestLine; j--) {
			for (int k = 0; k < stateVector[0].length; k++) {
				// System.out.println("@ rcl j"+j);
				stateVector[j][k] = stateVector[j - 1][k];
			}

		}

		// clear topmost line
		for (int k = 0; k < stateVector[0].length; k++) {
			stateVector[currentHighestLine][k] = 0;
		}

	}

}