/**
 * This class represents TShape shape and has a predefined shape bit pattern
 */

/**
 * @author Admin
 *
 */
public class TShape extends Shape {
	/**
	 * pre define shape bit pattern
	 */
	public TShape() {

		Integer[][] theShapeBitPattern = { { 1, 1, 1, 0 }, { 0, 1, 0, 0 },
				{ 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

		super.setShapeBitPattern(theShapeBitPattern);
	}
}