/**
 * This Class represents the preview panel to show the next Shape on the game GUI
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 * Add one sentence class summary here. Add class description here.
 * 
 * @author SRINIVASA REDDY B.R (2007119)
 * @version 1.0, Apr 16, 2008
 */
public class PreviewPanel extends JPanel {

	Integer[][] shapeBitPattern;

	Controller controller;

	static int width = 250, height = 250;

	static int XCells = 5, YCells = 5;

	static int blockWidth = 25, blockHeight = 25;

	int gameOver = 0;

	static int blockSizeX = 4;

	static int blockSizeY = 4;

	int posx = 0, posy = 0;

	public PreviewPanel(Controller controller) {
		setBackground(Color.black);
		setBorder(new LineBorder(Color.WHITE, 1, false));
		this.controller = controller;
	}

	/**
	 * This method is automatically called by JVM passing a graphics of the panel
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		this.shapeBitPattern = controller.getNextShape().getShapeBitPattern();
		// displayAll();

		int posxDupe = 1;
		int posyDupe = 1;

		Graphics2D g2 = (Graphics2D) g;
		g2.setPaint(Color.cyan);

		// paint a shape on Panel
		for (int i = 0; i < blockSizeX; i++) {
			for (int j = 0; j < blockSizeY; j++) {
				int x = posxDupe * blockWidth;
				int y = posyDupe * blockHeight;
				if (shapeBitPattern[i][j] == 1)
					g2.fillRoundRect(x, y, blockWidth, blockHeight, 10, 10);
				// g2.clearRect(x,y,blockWidth,blockHeight);
				posxDupe++;
			}
			posyDupe++;
			posxDupe = 1;
		}
	}

	
}
