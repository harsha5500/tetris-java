/**
 * This class represents Left Movement and has  verify and movement methods
*/


/**
 * @author Admin
 * 
 */
public class Left implements Move {
	static Controller controller;

	/**
	 * reduce the X position of the shape location by 1
	 */

	public void movement() {
		controller = Controller.getInstance();
		if (verify()) {
			int xPos = controller.getShapeLocation().getX();
			xPos -= 1;
			if (xPos < 0)
				xPos = 0;
			controller.getShapeLocation().setX(xPos);
		}
	}

	/**
	 * verify movable
	 */
	public boolean verify() {
		Integer[][] canvasStateVector = ((GameCanvas) controller.getCanvas())
				.getStateVector();
		Integer[][] shapeBitPattern = controller.getCurrentShape()
				.getShapeBitPattern();
		int xPos = controller.getShapeLocation().getX();
		int yPos = controller.getShapeLocation().getY();
		if (xPos == 0)
			return false;

		for (int i = 0; i < MyGamePanel.blockSizeY; i++) {
			// System.out.println((controller.shapeBottomEdgePosition[i]+1) +"
			// >= "+ (controller.shapeFinalPosition[i]));
			if (shapeBitPattern[i][0] == 1
					&& canvasStateVector[yPos + i][xPos - 1] == 1)

				return false;
		}

		return true;
	}

}