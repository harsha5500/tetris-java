/**
 * This class represents the game panel that has the canvas and current object
 *
 */

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 * Add one sentence class summary here. Add class description here.
 * 
 * @author SRINIVASA REDDY B.R (2007119)
 * @version 1.0, Apr 7, 2008
 */
public class MyGamePanel extends JPanel {

	Integer[][] stateVector;

	Integer[][] shapeBitPattern;

	Controller controller;

	static int width = 450, height = 600;

	
	static int XCells = 14, YCells = 20;

	static int blockWidth = 25, blockHeight = 25;

	int gameOver = 0;

	static int blockSizeX = 4;

	static int blockSizeY = 4;

	int posx = 0, posy = 0;

	public MyGamePanel(Controller controller) {
		setBackground(Color.black);
		setBorder(new LineBorder(Color.WHITE, 1, false));
		this.controller = controller;
	}

	public void setStateVector(Integer[][] stateVector) {
		this.stateVector = stateVector;
	}

	public void setShapeBitPattern(Integer[][] shapeBitPattern) {
		this.shapeBitPattern = shapeBitPattern;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		// System.out.println(controller);
		
		/*CHANGE 1*/
		String myRoll = "*043";
		this.stateVector = ((GameCanvas) controller.getCanvas())
				.getStateVector();
		this.shapeBitPattern = controller.getCurrentShape()
				.getShapeBitPattern();
		// displayAll();

		int posxDupe = controller.getShapeLocation().getX();
		int posyDupe = controller.getShapeLocation().getY();

		Graphics2D g2 = (Graphics2D) g;
		g2.setPaint(Color.red);

		// paint canvas on panel
		for (int i = 0; i < stateVector.length; i++) {
			for (int j = 0; j < stateVector[0].length; j++) {
				int x = j * blockWidth;
				int y = i * blockHeight;
				
				/*CHANGE 2*/
				if((i+1)==20)
				{
					//g2.setPaint(Color.red);
					g2.drawString(""+j, x, y);
					continue;
				}
				
				g2.drawString(""+i, 15, y);
				
				

				if (stateVector[i][j] == 1)
					
					/*CHANGE 1 : commented the fillRoundRect to draw String*/
					//g2.fillRoundRect(x, y, blockWidth, blockHeight, 10, 10);
					g2.drawString(myRoll, x, y);
			}
		}

		// paint a shape on Panel
		for (int i = 0; i < blockSizeX; i++) {
			for (int j = 0; j < blockSizeY; j++) {
				int x = posxDupe * blockWidth;
				int y = posyDupe * blockHeight;
				if (shapeBitPattern[i][j] == 1) {
					g2.setColor(Color.red);

					/*CHANGE 1*/
					//g2.fillRoundRect(x, y, blockWidth, blockHeight, 10, 10);
					g2.drawString(myRoll, x, y);
				}
				// g2.clearRect(x,y,blockWidth,blockHeight);
				posxDupe++;
			}
			posyDupe++;
			posxDupe = controller.getShapeLocation().getX();
		}

		// paint shadow on panel
		if (controller != null && GameSetting.getInstance() != null) {
			if (controller.getShadowFlag() == true) {

				controller.setShapeFinalPosition();

				Integer[] shapeFinalPosition = controller.shapeFinalPosition;
				Integer[] shapeBottomPosition = Util
						.getShapeBottomPosition(controller.getCurrentShape());
				// Util.displayArray(shapeFinalPosition);
				// Util.displayArray(shapeBottomPosition);

				int shapeWidth = Util.getShapeWidth(controller
						.getCurrentShape());
				int shapeHeight = Util.getShapeHeight(controller
						.getCurrentShape());

				Integer[] difference = new Integer[shapeWidth];
				for (int i = 0; i < difference.length; i++) {
					if (shapeFinalPosition[i] != null)
						difference[i] = shapeFinalPosition[i]
								- shapeBottomPosition[i];
				}

				int yloc = Util.getMinOfArray(difference);

				posxDupe = controller.getShapeLocation().getX();
				posyDupe = yloc - 1;

				// System.out.println(" yloc "+yloc +" posy "+posyDupe);

				for (int i = 0; i < shapeHeight; i++) {
					for (int j = 0; j < shapeWidth; j++) {
						int x = posxDupe * blockWidth;
						int y = posyDupe * blockHeight;
						if (shapeBitPattern[i][j] == 1) {
							g2.setColor(Color.red);
							/*CHANGE 1*/
							//g2.drawRoundRect(x, y, blockWidth, blockHeight, 10,10);
									g2.drawString(myRoll,x,y);
						}
						// g2.clearRect(x,y,blockWidth,blockHeight);
						posxDupe++;
					}
					posyDupe++;
					posxDupe = controller.getShapeLocation().getX();
				}

			}
		}

	}

	public void clear(int x, int y, int w, int h) {
		// repaint(x, y,w, h);
	}

}
