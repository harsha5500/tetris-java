/**
 * This class represents InvertedLShape shape and has a predefined shape bit pattern
*/

/**
 * @author Admin
 * @generated "UML to Java V5.0
 *            (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
 */
public class InvertedLShape extends Shape {
	/**
	 *pre define shape bitvector
	 */
	public InvertedLShape() {
		Integer[][] theShapeBitPattern = { { 1, 1, 0, 0 }, { 1, 0, 0, 0 },
				{ 1, 0, 0, 0 }, { 0, 0, 0, 0 } };

		super.setShapeBitPattern(theShapeBitPattern);
	}
}